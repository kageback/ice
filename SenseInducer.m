classdef SenseInducer < handle
	properties (SetAccess = private, GetAccess = private)
        p
        wordvectorcont
        contextvectorcont
        textfilename
        contextWidth
        maxNumOfInstanses
        kTests
        clusAlgo
        filterCoeff
        dim
        map
        statmap
        clustEval
        clustDist
    end
    methods
        function this = SenseInducer(tmp, textfilename, wordvectorcont, contextvectorcont, varargin)
        %SenseInducer Induces word senses from text.
        %   Induces word senses using skip-gram vectors and a text corpus.
           

            %Vars
            this.wordvectorcont = wordvectorcont;
            this.contextvectorcont = contextvectorcont;
            this.dim = length(wordvectorcont.get(''));
            
            this.textfilename = textfilename;
            
            %Params
         	ip = inputParser;
            addParameter(ip,'encoding','ISO-8859-1');
            addParameter(ip,'blocksize',100000);
            addParameter(ip,'randomInstances',true);
            addParameter(ip,'requireStandardContext',true);
            addParameter(ip,'standardContextCutoff',0.7);
            addParameter(ip,'contextWidth',10);
            addParameter(ip,'maxNumOfInstanses',10);%1800
            addParameter(ip,'kTests',2:5);
            addParameter(ip,'clustAlgo','kmeans');
            addParameter(ip,'filterCoeff',NaN);
            addParameter(ip,'clustEval','pham');
            addParameter(ip,'clustDist','sqeuclidean');
            addParameter(ip,'multiSense',false);
            addParameter(ip,'multiSenseCutOff',0.95);

            parse(ip,varargin{:});
            this.p = ip.Results;
            
            this.contextWidth = this.p.contextWidth;
            this.maxNumOfInstanses = this.p.maxNumOfInstanses;
            this.kTests = this.p.kTests;
            this.clusAlgo = this.p.clustAlgo;
            this.filterCoeff = this.p.filterCoeff;

            
            
            this.clustEval = this.p.clustEval;
            this.clustDist = this.p.clustDist;
            this.map = containers.Map();
            this.statmap = containers.Map();
    
        end
        function res = tostring(this)
         res = sprintf('Parameters for SenseInducer -> \n Corpus:%s %s\n',this.textfilename,struct2str(this.p));
        end
    %%
    function [senses, stats] = induceSenses(this, targetWord, testInstances)
        %% compute instance vectors
        if isKey(this.map,targetWord)
            senses = this.map(targetWord);
            stats = this.statmap(targetWord);
            return;
        end
        [ instanceVectors, stats ] = computeInstanceVectors(this,...
                                                            this.textfilename , ...
                                                            this.wordvectorcont,...
                                                            this.contextvectorcont,... 
                                                            targetWord,...
                                                            this.contextWidth,...
                                                            this.maxNumOfInstanses);
                                                
        if nargin>2
           instanceVectors = [testInstances'; instanceVectors];
        end

        %% Cluster
        if strcmp(this.clustEval,'pham')
            eva = evalclusters_pham(instanceVectors', this.kTests, this.clustDist);
        else
            eva = evalclusters(instanceVectors, this.clusAlgo,'CalinskiHarabasz','klist',this.kTests);
        end
        
        centroids = zeros(eva.OptimalK,this.dim);
        Ids = cell(1,eva.OptimalK);
        for k = 1:eva.OptimalK
            centroids(k,:) = mean(instanceVectors(eva.OptimalY == k,:));
            Ids{k} = ['s' num2str(k)];
        end

        %% 
        senses = Senses(this.p,targetWord, centroids, Ids, eva.OptimalY);
        this.map(targetWord) = senses;
        this.statmap(targetWord) = stats;
        
    end
    
    %%
    function [senses] = induceNSenses(this, targetWord, N)
         %% compute instance vectors
        if isKey(this.map,targetWord)
            senses = this.map(targetWord);
            return;
        end
        [ instanceVectors, stats ] = computeInstanceVectors(this.textfilename , ...
                                                            this.wordvectorcont,...
                                                            this.contextvectorcont,... 
                                                            targetWord,...
                                                            this.contextWidth,...
                                                            this.maxNumOfInstanses);
        

        %% Cluster
        [idx,centroids] = kmeans(instanceVectors,N);
        
%         if strcmp(this.p.clustEval,'pham')
%             eva = evalclusters_pham(instanceVectors', this.kTests, this.p.clustDist);
%         else
%             eva = evalclusters(instanceVectors, this.clusAlgo,'CalinskiHarabasz','klist',this.kTests);
%         end
%         
%         centroids = zeros(eva.OptimalK,this.dim);
%         Ids = cell(1,eva.OptimalK);
%         for k = 1:eva.OptimalK
%             centroids(k,:) = mean(instanceVectors(eva.OptimalY == k,:));
%             Ids{k} = ['s' num2str(k)];
%         end

        %% 
        Ids = 1:size(centroids,1);
        senses = Senses(this.p,targetWord, centroids, Ids, N);
        this.map(targetWord) = senses;
        this.statmap(targetWord) = stats;
    end
    function [ instanceVectors, stats] = computeInstanceVectors(this, filename, wordvectorcont, contextvectorcont, targetWord, contexwidth, maxNumOfInstanses,varargin)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    
maxwordlen = 50;

collectStats = false;
if nargout>1
    stats = struct();
    stats.instanceVectors = {};
    stats.overlapFactors = {};
    stats.contexts = {};
    collectStats = true;
end

targetVector = wordvectorcont.get(targetWord);
%dim = length(targetVector);
instanceVectors = zeros(maxNumOfInstanses,this.dim);

fid = fopen(filename,'r','n',this.p.encoding);

% fseek(fid,0,'eof');
% eofPos = ftell(fid);
% frewind(fid);

fstat = dir(filename);
fsize = fstat.bytes;

iInst=1;
while ~feof(fid) && iInst <= maxNumOfInstanses
    
    if this.p.randomInstances
        spos = randi(fsize-this.p.blocksize)-1;
        fseek(fid,spos,'bof');
    end
    
    %fprintf(' Progress: %0.1f%%\n',iInst/maxNumOfInstanses*100);%(ftell(fid)/eofPos)*100);
    [data, count] = fread(fid,this.p.blocksize,'*char');
    data = data';
    
    ind = strfind(data, [' ' targetWord ' ']);
    
    if ~isempty(ind)
        ind = ind(1);
    end
    for i=ind
        if i > (contexwidth*maxwordlen) && i + (contexwidth*maxwordlen) < count
            leftTok = strsplit(data(i-(contexwidth*maxwordlen):i-1),' ');
            rightTok = strsplit(data(i+1:i+(contexwidth*maxwordlen)),' ');
            
            strcontext = [leftTok(end-contexwidth+1:end),rightTok(2:contexwidth+1)];
            
            if this.p.requireStandardContext
                numStdWords = sum(~cellfun(@isempty, regexp(strcontext, '^[a-z]+$')));
                if numStdWords/length(strcontext) < this.p.standardContextCutoff;
                    break;
                end
            end
            
            [instanceVector, ds] = computeInstanceVector(targetVector, strcontext, contextvectorcont);
            instanceVectors(iInst,:) = instanceVector';
            
            if collectStats
                stats.instanceVectors{end+1} = instanceVector;
                stats.overlapFactors{end+1} = ds;
                stats.contexts{end+1} = strcontext;
            end
            
            iInst = iInst+1;
            if iInst > maxNumOfInstanses
                break;
            end
        end

    end
    
end
fclose(fid);
end
    

        end
end
