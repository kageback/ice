classdef StatusWriter < handle
%StatusWriter Used to write static and dynamic messages to the console.
    properties (SetAccess = private, GetAccess = private)
        statusTextLength
    end
    properties (SetAccess = private)
        printStatic
    end
    methods
        function obj = StatusWriter()
            obj.statusTextLength = 0;
            obj.printStatic = @fprintf;
        end
        
        function printDynamic(obj, statusText)
        %printDynamic prints an overwritable message to console
        
            fprintf(repmat('\b',1,obj.statusTextLength));
            obj.statusTextLength = length(statusText);

            disp(statusText);
            fprintf('\b');
        end
        
        function printProgress(obj,i, max, min)
        %printProgress Prints progress (in percent) as a dynamic message.
            if nargin == 2
                progress = i*100;
            elseif nargin == 3
                progress = (i/max)*100;
            elseif nargin == 4
                progress = ((i-min)/(max-min))*100;
            end

            obj.printDynamic(sprintf('%0.0f%% ',progress));

        end

        function lineFeed(obj)
        %lineFeed moves writer to next line, leaving dynamic messages as
        %static.
             fprintf('\n');
             obj.statusTextLength = 0;
        end

        function clearDynamic(obj)
        %clearDynamic removes current dynamic message.
            obj.printDynamic('');
        end
    end
end

