function S = struct2str(opt)

S = struct2str_rec(opt,0);

end

function S = struct2str_rec(opt,l)

structNames = {};
structOpt = {};
stringOpt = {};
fields = fieldnames(opt);

S = '';

sp = repmat(' ',1,2*l);
spn = repmat(' ',1,2*(l+1));

maxl = 0;
for i = 1:numel(fields)
    maxl = max(maxl,length(fields{i}));
end

for i = 1:numel(fields)
    v = opt.(fields{i});
    fieldname = [repmat(' ',1,maxl-length(fields{i})),fields{i}];
    if(isstruct(v) && length(v)==1)
        Si = struct2str_rec(v,l+1);
        structNames{end+1} = fields{i};
        structOpt{end+1} = Si;
    elseif(isstruct(v) && length(v)>1)
        S = sprintf('%s\n%s%s: <struct array>',S,sp,fieldname);
    elseif(iscell(v) && length(v)>1)
        S = sprintf('%s\n%s%s: <cell array>',S,sp,fieldname);
    elseif(isnumeric(v) && length(v)>1)
        S = sprintf('%s\n%s%s: %s',S,sp,fieldname,mat2str(v));
    elseif(ischar(v))
        stringOpt{end+1} = {fields{i},v};
        S = sprintf('%s\n%s%s: %s',S,sp,fieldname,v);
    else
        S = sprintf('%s\n%s%s: %s',S,sp,fieldname,strtrim(evalc(['disp(v)'])));
    end
end

for i=1:length(structOpt)
    S = sprintf('%s\n\n%s.%s: (sub-struct)%s%s',S,spn,structNames{i},evalc(['disp(structOpt{i})']));
end

end