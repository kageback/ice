function eva = evalclusters_pham(X,ks,distancemeasure)

if nargin<3
    distancemeasure = 'sqeuclidean';
end

dim = size(X,1);

S = zeros(max(ks),1);
IDs = cell(max(ks),1);
Cs = cell(max(ks),1);
navg = 1;
for k=1:max(ks)
    for iavg=1:navg
        [ID,C] = kmeans(X',k,'Distance',distancemeasure);
        IDs{k} = ID;
        Cs{k} = C;
        C = C';
        I = zeros(k,1);
        for j=1:k
            n = sum(ID==j);
            I(j) = sum(sum((X(:,ID==j)-repmat(C(:,j),1,n)).^2));
        end
        S(k) = sum(I);
    end
    S(k) = S(k)/navg;
end

f = zeros(max(ks),1);
a = zeros(max(ks),1);
f(1) = 1;
a(2) = 1-3/(4*dim);

for k=2:max(ks)
    if k>2
        a(k) = a(k-1) + (1-a(k-1))/6;
    end
    if S(k-1)==0
        f(k) = 1;
    else
        f(k) = S(k)/(a(k)*S(k-1));
    end
end

[minf, minf_i] = min(f(ks));

eva = struct();
eva.OptimalK = ks(minf_i);
eva.OptimalY = IDs{eva.OptimalK};
eva.OptimalC = Cs{eva.OptimalK};
eva.scores = f;