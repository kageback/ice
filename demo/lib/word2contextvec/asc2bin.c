#include <stdio.h>
#include <string.h>
#include <math.h>
#include <malloc.h>

const long long max_size = 2000;         // max length of strings
const long long N = 40;                  // number of closest words that will be shown
const long long max_w = 50*4;              // max length of vocabulary entries

int main(int argc, char **argv) {

    char str [max_w];
    long long vocab_size, layer1_size;
    float num;

    FILE *f, *fo;
    char file_name_in[max_size];
    char file_name_out[max_size];

    if (argc < 2) {
        printf("Usage: asc2bin|bin2asc <INPUT_FILE> <OUTPUT_FILE>\n");
        return 0;
    } else {
        strcpy(file_name_in,argv[1]);
        strcpy(file_name_out,argv[2]);
    }

    f = fopen(file_name_in, "rb");
    if (f == NULL) {
        printf("Input file not found\n");
        return -1;
    }

    fo = fopen(file_name_out, "wb");
    if (fo == NULL) {
        printf("Could not create output file\n");
        return -1;
    }

    //copy the metadata
    fscanf(f, "%lld", &vocab_size);
    fscanf(f, "%lld", &layer1_size);
    fprintf(fo, "%lld %lld\n", vocab_size, layer1_size);

    int b;
    for (b = 0; b < vocab_size; b++) {
        fscanf (f, "%s", str);
        fprintf(fo,"%s ",str);

        fgetc(f);
        int i;
        // copy vector in ascii format and write bin
        for (i = 0; i < layer1_size; i++) {
            #ifdef inverse
                num = 0;
                fread(&num, sizeof(num), 1, f);
                fprintf(fo, "%lf ", num);
            #else
                fscanf(f, "%f", &num);
                fwrite(&num, sizeof(float), 1, fo);
            #endif

        }
        fgetc(f);
        fprintf(fo, "\n");
    }
    fclose(f);
    fclose(fo);

  return 0;
}
