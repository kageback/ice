#!/bin/sh

## Fetch dataset
mkdir data
cd data

if [ ! -e text8 ]; then
  wget http://mattmahoney.net/dc/text8.zip -O text8.gz
  gzip -d text8.gz -f
fi

cd ..

## Compute skip-gram vectors
mkdir bin

(cd lib/word2contextvec/ && make word2contextvec)

./lib/word2contextvec/word2contextvec -train ./data/text8 -output bin/vectors.bin -contex-output bin/contextvectors.bin -save-vocab bin/vocab.txt -cbow 0 -size 300 -window 10 -negative 10 -hs 0 -sample 1e-5 -threads 40 -binary 1 -iter 3 -min-count 10

(cd lib/word2contextvec/ && make clean)

## Run WSI script
matlab -nodisplay -r "run('demo.m');exit;"
