function [instanceVector, d] = computeInstanceVector(targetVector, strcontext, contextvectorcont, varargin)
%computeInstanceVector Summary of this function goes here
%   Detailed explanation goes here
    ip = inputParser;
    addParameter(ip,'useICC',true);
    addParameter(ip,'useContextWindow',true);
    parse(ip,varargin{:});
    p = ip.Results;
    
    contextvectors = zeros(length(strcontext),contextvectorcont.dim);
    for k=1:length(strcontext)
        contextvectors(k,:) = contextvectorcont.get(strcontext{k});
    end

    try
        d = 1./(1+exp(-contextvectors*targetVector)).*(sum(contextvectors,2)~= 0);
    catch
        keyboard
    end
    if p.useICC
        contextvectors = repmat(d,1,size(contextvectors,2)) .* contextvectors;
    end
    if p.useContextWindow
        contextwidth = ceil(length(strcontext)/2);
        window = [ 1/contextwidth:1/contextwidth:1 1:-1/contextwidth:1/contextwidth]';
        window = window(1:length(strcontext));
        contextvectors = repmat(window,1,size(contextvectors,2)) .* contextvectors;
    end

    instanceVector = sum(contextvectors,1)';
    instanceVector = instanceVector / norm(instanceVector);
end

